<h1 align="center">Disbursment to Slightly Big Flip API</h1>

Sebelumnya pastikan terlebih dahulu kebutuhan tersebut tersedia dan berjalan :

<ul>
<li>
Composer
</li>
<li>
PHP
</li>
<li>
MySQL
</li>

<h2>Instalasi </h2>
buka terminal pada directory kemudian jalankan :
<ul>
<li>
composer install
</li>

<li>
cp .env.example .env
</li>

<li>
php artisan migrate
</li>

<li>
php artisan serve
</li>

<li>
php artisan serve
</li>

<li>
<a> Jika Berhasil maka Url ini dapat dijalankan: http://127.0.0.1:8000</a>
</li>
</ul>

<h2>Dokumentasi API</h2>
jika instalasi dan aplikasi sudah berjalan, kita dapat menggunakan/ melihat dokumentasi api dibawah ini :

<ul>
<li>
<a>http://127.0.0.1:8000/api/docs</a>
</li>

<li>
<a>http://127.0.0.1:8000/api/docs#/Payment/api.payment.create</a>
untuk mengirim data disbursment
</li>

<li>
<a>http://127.0.0.1:8000/api/docs#/Payment/api.payment.check</a>
untuk mengecek dan update data disbursement terbaru
</li>
</ul>
