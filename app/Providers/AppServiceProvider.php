<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use GuzzleHttp\Client;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $baseUrl = env('NEXTAR_FLIP_BASE_URL');
        $this->app->singleton('GuzzleHttp\Client', function ($api) use ($baseUrl) {
            return new Client([
                'base_uri' => $baseUrl,
                'auth' => [env('USERNAME_SLIGHTY_BIG_FLIP'), '']
            ]);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
