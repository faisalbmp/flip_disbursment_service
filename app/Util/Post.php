<?php

namespace App\Util;

use GuzzleHttp\Client;

class Post
{
  protected $client;

  public function __construct(Client $client)
  {
    $this->client = $client;
  }

  public function create($body)
  {
    return $this->endpointRequest('POST', '/disburse', $body);
  }

  public function findById($id)
  {
    return $this->endpointRequest("GET", "/disburse/" . $id);
  }

  public function endpointRequest($method = 'GET', $url, $body = null)
  {
    try {
      $response = $this->client->request($method, $url, [
        'form_params' => $body
      ]);
    } catch (\Exception $e) {
      return $e;
    }

    return $this->response_handler($response->getBody()->getContents());
  }

  public function response_handler($response)
  {
    if ($response) {
      return json_decode($response);
    }

    return [];
  }
}
