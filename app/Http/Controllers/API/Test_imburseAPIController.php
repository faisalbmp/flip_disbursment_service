<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTest_imburseAPIRequest;
use App\Http\Requests\API\UpdateTest_imburseAPIRequest;
use App\Models\Test_imburse;
use App\Repositories\Test_imburseRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\Test_imburseResource;
use Response;

/**
 * Class Test_imburseController
 * @package App\Http\Controllers\API
 */

class Test_imburseAPIController extends AppBaseController
{
    /** @var  Test_imburseRepository */
    private $testImburseRepository;

    public function __construct(Test_imburseRepository $testImburseRepo)
    {
        $this->testImburseRepository = $testImburseRepo;
    }

    /**
     * Display a listing of the Test_imburse.
     * GET|HEAD /testImburses
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $testImburses = $this->testImburseRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(Test_imburseResource::collection($testImburses), 'Test Imburses retrieved successfully');
    }

    /**
     * Store a newly created Test_imburse in storage.
     * POST /testImburses
     *
     * @param CreateTest_imburseAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTest_imburseAPIRequest $request)
    {
        // $input = $request->all();
        var_dump($request);
        // $testImburse = $this->testImburseRepository->create($input);

        // return $this->sendResponse(new Test_imburseResource($testImburse), 'Test Imburse saved successfully');
    }

    /**
     * Display the specified Test_imburse.
     * GET|HEAD /testImburses/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Test_imburse $testImburse */
        $testImburse = $this->testImburseRepository->find($id);

        if (empty($testImburse)) {
            return $this->sendError('Test Imburse not found');
        }

        return $this->sendResponse(new Test_imburseResource($testImburse), 'Test Imburse retrieved successfully');
    }

    /**
     * Update the specified Test_imburse in storage.
     * PUT/PATCH /testImburses/{id}
     *
     * @param int $id
     * @param UpdateTest_imburseAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTest_imburseAPIRequest $request)
    {
        $input = $request->all();

        /** @var Test_imburse $testImburse */
        $testImburse = $this->testImburseRepository->find($id);

        if (empty($testImburse)) {
            return $this->sendError('Test Imburse not found');
        }

        $testImburse = $this->testImburseRepository->update($input, $id);

        return $this->sendResponse(new Test_imburseResource($testImburse), 'Test_imburse updated successfully');
    }

    /**
     * Remove the specified Test_imburse from storage.
     * DELETE /testImburses/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Test_imburse $testImburse */
        $testImburse = $this->testImburseRepository->find($id);

        if (empty($testImburse)) {
            return $this->sendError('Test Imburse not found');
        }

        $testImburse->delete();

        return $this->sendSuccess('Test Imburse deleted successfully');
    }
}
