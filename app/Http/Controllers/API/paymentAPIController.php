<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatepaymentAPIRequest;
use App\Http\Requests\API\UpdatepaymentAPIRequest;
use App\Models\payment;
use App\Repositories\paymentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\paymentResource;
use App\Util\Post;
use Response;

/**
 * Class paymentController
 * @package App\Http\Controllers\API
 */

class paymentAPIController extends AppBaseController
{
    /** @var  paymentRepository */
    private $paymentRepository;
    protected $post;

    public function __construct(paymentRepository $paymentRepo, Post $post)
    {
        $this->paymentRepository = $paymentRepo;
        $this->post = $post;
    }

    /**
     * Display a listing of the payment.
     * GET|HEAD /payments
     *
     * @param Request $request
     * @return Response
     */

    public function index(Request $request)
    {
        $payments = $this->paymentRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(paymentResource::collection($payments), 'Payments retrieved successfully');
    }


    /**
     *
     * @SWG\Post(
     * 		 path="/disbursment",
     *     description="Sent Payment Disbursment.",
     *     operationId="api.payment.create",
     *     produces={"application/json"},
     *     tags={"Payment"},
     *      @SWG\Parameter(
     *       name="account_number",
     *       description="User Acc Number",
     *       required=true,
     * 			in="body",
	 * 			@SWG\Schema(),
     *		),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     ),
     * 	)
     *
     */
    public function store(CreatepaymentAPIRequest $request)
    {
        $input = $request->all();

        // fetch send disbursment data
        $data = $this->post->create($input);
        $data->transaction_id = $data->id;
        $payment = $this->paymentRepository->create($data);

        return $this->sendResponse(new paymentResource($payment), 'Payment saved successfully');
    }

    /**
     * Display the specified payment.
     * GET|HEAD /payments/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var payment $payment */
        $payment = $this->paymentRepository->find($id);

        if (empty($payment)) {
            return $this->sendError('Payment not found');
        }

        return $this->sendResponse(new paymentResource($payment), 'Payment retrieved successfully');
    }

    /**
     * Update the specified payment in storage.
     * PUT/PATCH /payments/{id}
     *
     * @param int $id
     * @param UpdatepaymentAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatepaymentAPIRequest $request)
    {
        $input = $request->all();

        /** @var payment $payment */
        $payment = $this->paymentRepository->find($id);

        if (empty($payment)) {
            return $this->sendError('Payment not found');
        }

        $payment = $this->paymentRepository->update($input, $id);

        return $this->sendResponse(new paymentResource($payment), 'payment updated successfully');
    }

    /**
     * Remove the specified payment from storage.
     * DELETE /payments/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var payment $payment */
        $payment = $this->paymentRepository->find($id);

        if (empty($payment)) {
            return $this->sendError('Payment not found');
        }

        $payment->delete();

        return $this->sendSuccess('Payment deleted successfully');
    }

    /**
     * Check the Payment Status.
     * GET|HEAD /payments/check/{id}
     *
     * @param int $id
     *
     * @return Response
     * 
     *
     * @SWG\Get(
     * 		 path="/disbursment/check/{id}",
     *     description="Check Payment Disbursment.",
     *     operationId="api.payment.check",
     *     produces={"application/json"},
     *     tags={"Payment"},
	 * 		@SWG\Parameter(
	 * 			name="id",
	 * 			in="path",
	 * 			required=true,
	 * 			type="string",
	 * 			description="Transaction_Id",
	 * 		),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     ),
     * 	)
     *
     */
    public function check($id)
    {
        /** @var payment $payment */
        $data = $this->post->findById($id);
        $data->transaction_id = $id;
        $payments = $this->paymentRepository->allQuery([
            "transaction_id" => $id
        ]);
        foreach ($payments->get() as $key => $value) {
            // dd($data);
            $payment = $this->paymentRepository->update(json_decode(json_encode($data), true), $value->id);
        }

        return $this->sendResponse(new paymentResource($payment), 'Payment saved successfully');
        if (empty($payment)) {
            return $this->sendError('Payment not found');
        }

        return $this->sendResponse(new paymentResource($payment), 'Payment retrieved successfully');
    }
}
