<?php

namespace App\Http\Requests\API;

use App\Models\Test_imburse;
use InfyOm\Generator\Request\APIRequest;

class UpdateTest_imburseAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Test_imburse::$rules;
        
        return $rules;
    }
}
