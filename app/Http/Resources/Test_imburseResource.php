<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Test_imburseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'bank_code' => $this->bank_code,
            'account_number' => $this->account_number,
            'amount' => $this->amount,
            'remark' => $this->remark,
            'status' => $this->status,
            'receipt' => $this->receipt,
            'time_served' => $this->time_served,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
