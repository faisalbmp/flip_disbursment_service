<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Test_imburse
 * @package App\Models
 * @version January 8, 2021, 7:22 pm UTC
 *
 * @property string $bank_code
 * @property string $account_number
 * @property integer $amount
 * @property string $remark
 * @property string $status
 */
class Test_imburse extends Model
{
    use SoftDeletes;

    public $table = 'test_imburses';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'bank_code',
        'account_number',
        'amount',
        'remark',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'bank_code' => 'string',
        'account_number' => 'string',
        'amount' => 'integer',
        'remark' => 'string',
        'status' => 'string',
        'receipt' => 'string',
        'time_served' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'bank_code' => 'required',
        'account_number' => 'required',
        'amount' => 'required'
    ];

    
}
