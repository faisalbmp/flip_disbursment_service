<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class payment
 * @package App\Models
 * @version January 10, 2021, 4:09 am UTC
 *
 * @property string $bank_code
 * @property string $account_number
 * @property integer $amount
 * @property string $remark
 * @property string $status
 * @property string $receipt
 * @property string $time_served
 * @property string $transaction_id
 */
class payment extends Model
{
    use SoftDeletes;

    public $table = 'payments';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'id' => 'integer',
        'bank_code',
        'account_number',
        'amount',
        'remark',
        'status',
        'receipt',
        'time_served',
        'transaction_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'bank_code' => 'string',
        'account_number' => 'string',
        'amount' => 'integer',
        'remark' => 'string',
        'status' => 'string',
        'receipt' => 'string',
        'time_served' => 'string',
        'transaction_id' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'bank_code' => 'required',
        'account_number' => 'required',
        'amount' => 'required',
        'remark' => 'required'
    ];
}
