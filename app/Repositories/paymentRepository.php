<?php

namespace App\Repositories;

use App\Models\payment;
use App\Repositories\BaseRepository;

/**
 * Class paymentRepository
 * @package App\Repositories
 * @version January 10, 2021, 4:09 am UTC
*/

class paymentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'bank_code',
        'account_number',
        'amount',
        'remark',
        'status',
        'receipt',
        'time_served',
        'transaction_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return payment::class;
    }
}
