<?php

namespace App\Repositories;

use App\Models\Test_imburse;
use App\Repositories\BaseRepository;

/**
 * Class Test_imburseRepository
 * @package App\Repositories
 * @version January 8, 2021, 7:22 pm UTC
*/

class Test_imburseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'bank_code',
        'account_number',
        'amount',
        'remark',
        'status',
        'receipt',
        'time_served'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Test_imburse::class;
    }
}
