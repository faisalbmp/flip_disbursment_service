<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Test_imburse;

class Test_imburseApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_test_imburse()
    {
        $testImburse = factory(Test_imburse::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/test_imburses', $testImburse
        );

        $this->assertApiResponse($testImburse);
    }

    /**
     * @test
     */
    public function test_read_test_imburse()
    {
        $testImburse = factory(Test_imburse::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/test_imburses/'.$testImburse->id
        );

        $this->assertApiResponse($testImburse->toArray());
    }

    /**
     * @test
     */
    public function test_update_test_imburse()
    {
        $testImburse = factory(Test_imburse::class)->create();
        $editedTest_imburse = factory(Test_imburse::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/test_imburses/'.$testImburse->id,
            $editedTest_imburse
        );

        $this->assertApiResponse($editedTest_imburse);
    }

    /**
     * @test
     */
    public function test_delete_test_imburse()
    {
        $testImburse = factory(Test_imburse::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/test_imburses/'.$testImburse->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/test_imburses/'.$testImburse->id
        );

        $this->response->assertStatus(404);
    }
}
