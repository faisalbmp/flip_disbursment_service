<?php namespace Tests\Repositories;

use App\Models\Test_imburse;
use App\Repositories\Test_imburseRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Test_imburseRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Test_imburseRepository
     */
    protected $testImburseRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->testImburseRepo = \App::make(Test_imburseRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_test_imburse()
    {
        $testImburse = factory(Test_imburse::class)->make()->toArray();

        $createdTest_imburse = $this->testImburseRepo->create($testImburse);

        $createdTest_imburse = $createdTest_imburse->toArray();
        $this->assertArrayHasKey('id', $createdTest_imburse);
        $this->assertNotNull($createdTest_imburse['id'], 'Created Test_imburse must have id specified');
        $this->assertNotNull(Test_imburse::find($createdTest_imburse['id']), 'Test_imburse with given id must be in DB');
        $this->assertModelData($testImburse, $createdTest_imburse);
    }

    /**
     * @test read
     */
    public function test_read_test_imburse()
    {
        $testImburse = factory(Test_imburse::class)->create();

        $dbTest_imburse = $this->testImburseRepo->find($testImburse->id);

        $dbTest_imburse = $dbTest_imburse->toArray();
        $this->assertModelData($testImburse->toArray(), $dbTest_imburse);
    }

    /**
     * @test update
     */
    public function test_update_test_imburse()
    {
        $testImburse = factory(Test_imburse::class)->create();
        $fakeTest_imburse = factory(Test_imburse::class)->make()->toArray();

        $updatedTest_imburse = $this->testImburseRepo->update($fakeTest_imburse, $testImburse->id);

        $this->assertModelData($fakeTest_imburse, $updatedTest_imburse->toArray());
        $dbTest_imburse = $this->testImburseRepo->find($testImburse->id);
        $this->assertModelData($fakeTest_imburse, $dbTest_imburse->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_test_imburse()
    {
        $testImburse = factory(Test_imburse::class)->create();

        $resp = $this->testImburseRepo->delete($testImburse->id);

        $this->assertTrue($resp);
        $this->assertNull(Test_imburse::find($testImburse->id), 'Test_imburse should not exist in DB');
    }
}
