<?php namespace Tests\Repositories;

use App\Models\payment;
use App\Repositories\paymentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class paymentRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var paymentRepository
     */
    protected $paymentRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->paymentRepo = \App::make(paymentRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_payment()
    {
        $payment = factory(payment::class)->make()->toArray();

        $createdpayment = $this->paymentRepo->create($payment);

        $createdpayment = $createdpayment->toArray();
        $this->assertArrayHasKey('id', $createdpayment);
        $this->assertNotNull($createdpayment['id'], 'Created payment must have id specified');
        $this->assertNotNull(payment::find($createdpayment['id']), 'payment with given id must be in DB');
        $this->assertModelData($payment, $createdpayment);
    }

    /**
     * @test read
     */
    public function test_read_payment()
    {
        $payment = factory(payment::class)->create();

        $dbpayment = $this->paymentRepo->find($payment->id);

        $dbpayment = $dbpayment->toArray();
        $this->assertModelData($payment->toArray(), $dbpayment);
    }

    /**
     * @test update
     */
    public function test_update_payment()
    {
        $payment = factory(payment::class)->create();
        $fakepayment = factory(payment::class)->make()->toArray();

        $updatedpayment = $this->paymentRepo->update($fakepayment, $payment->id);

        $this->assertModelData($fakepayment, $updatedpayment->toArray());
        $dbpayment = $this->paymentRepo->find($payment->id);
        $this->assertModelData($fakepayment, $dbpayment->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_payment()
    {
        $payment = factory(payment::class)->create();

        $resp = $this->paymentRepo->delete($payment->id);

        $this->assertTrue($resp);
        $this->assertNull(payment::find($payment->id), 'payment should not exist in DB');
    }
}
