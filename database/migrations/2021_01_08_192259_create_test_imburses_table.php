<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTestImbursesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_imburses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bank_code', 25);
            $table->string('account_number', 25);
            $table->integer('amount');
            $table->text('remark');
            $table->string('status', 25);
            $table->string('receipt', 25);
            $table->string('time_served', 25);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('test_imburses');
    }
}
