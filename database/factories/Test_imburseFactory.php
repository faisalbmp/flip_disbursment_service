<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Test_imburse;
use Faker\Generator as Faker;

$factory->define(Test_imburse::class, function (Faker $faker) {

    return [
        'bank_code' => $faker->word,
        'account_number' => $faker->word,
        'amount' => $faker->randomDigitNotNull,
        'remark' => $faker->text,
        'status' => $faker->word,
        'receipt' => $faker->word,
        'time_served' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
